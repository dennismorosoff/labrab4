Rails.application.routes.draw do
  root  'static_pages#findmaterial'

  get 'static_pages/home'
  get 'static_pages/help'
  get 'static_pages/about'
  get 'static_pages/findmaterial'
  post 'static_pages/findmaterial'

  match '/help',    to: 'static_pages#help',    via: 'get'
  match '/about',   to: 'static_pages#about',   via: 'get'
  match '/findmaterial',   to: 'static_pages#findmaterial',   via: 'get'
  match '/findmaterial',   to: 'static_pages#findmaterial',   via: 'post'

  get '/contact', to: redirect('https://vk.com/write13246897')

  resources :propunits
  resources :matprops
  resources :materials
  resources :gosts
  resources :matpropvalues

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
