require 'rails_helper'

describe "Static pages" do

  describe "Home page" do

    it "should have the content 'Sample App'" do
      visit '/static_pages/home'
      expect(page).to have_content('Sample App')
    end
  end
  
  describe "Help page" do

    it "should have the content 'помощь'" do
      visit '/static_pages/help'
      expect(page).to have_content('помощь')
    end
  end

  describe "About page" do

    it "should have the content 'блог'" do
      visit '/static_pages/about'
      expect(page).to have_content('блог')
    end
  end

  describe "Check all titles" do
  	  
    it "Home should have the right title" do
      visit '/static_pages/home'
      expect(page).to have_title("Главная | Home")
    end

    it "Help should have the right title" do
      visit '/static_pages/help'
      expect(page).to have_title("Помощь | Help")
    end

    it "About should have the right title" do
      visit '/static_pages/about'
      expect(page).to have_title("Обо мне | About me")
    end

end

end