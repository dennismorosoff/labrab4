class MatpropvaluesController < ApplicationController

  def new
    @matpropvalue = Matpropvalue.new
    @matprops = Matprop.all
    @materials = Material.all.order(:name)
  end

  helper_method :sort_column, :sort_direction

  def index
    @matpropvalues = Matpropvalue.order(sort_column + ' ' + sort_direction)
  end

  def edit
    @matpropvalue = Matpropvalue.find(params[:id])

    @matprops = Matprop.all
    @materials = Material.all.order(:name)
    @matprop = Matprop.find(@matpropvalue.property_id)
    @material = Material.find(@matpropvalue.material_id)
  end

  def show
    @matpropvalue = Matpropvalue.find(params[:id])

    @matprop = Matprop.find(@matpropvalue.property_id)
    @propunit = Propunit.find(@matprop.unit_id)
    @material = Material.find(@matpropvalue.material_id)
  end

  def create
    # render plain: params[:value].inspect

    @matpropvalue = Matpropvalue.new(matpropvalue_params)

    if @matpropvalue.save
      redirect_to @matpropvalue
    else
      render 'new'
    end

  end

  def update
    @matpropvalue = Matpropvalue.find(params[:id])

    if @matpropvalue.update(matpropvalue_params)
      redirect_to @matpropvalue
    else
      render 'edit'
    end
  end

  def destroy
    @matpropvalue = Matpropvalue.find(params[:id])
    @matpropvalue.destroy

    redirect_to matpropvalues_path
  end

  private
  def matpropvalue_params
    params.require(:matpropvalue).permit(:material_id, :property_id, :num_value)
  end

  def sort_column
    Matpropvalue.column_names.include?(params[:sort]) ? params[:sort] : "id"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ?  params[:direction] : "asc"
  end
end
