class MatpropsController < ApplicationController

  def new
    @matprop = Matprop.new
    @propunits = Propunit.all.order(:name)
  end

  helper_method :sort_column, :sort_direction

  def index
    @matprops = Matprop.order(sort_column + ' ' + sort_direction)
  end

  def edit
    @matprop = Matprop.find(params[:id])
    @propunit = Propunit.find(@matprop.unit_id)
    @propunits = Propunit.all.order(:name)
  end

  def show
    @matprop = Matprop.find(params[:id])
    @propunit = Propunit.find(@matprop.unit_id)
  end

  def create
    #   render plain: params[:propunit].inspect

    @matprop = Matprop.new(matprop_params)

    if @matprop.save
      redirect_to @matprop
    else
      render 'new'
    end
  end

  def update
    @matprop = Matprop.find(params[:id])

    if @matprop.update(matprop_params)
      redirect_to @matprop
    else
      render 'edit'
    end
  end

  def destroy
    @matprop = Matprop.find(params[:id])
    @matprop.destroy

    redirect_to matprops_path
  end

  private
  def matprop_params
    params.require(:matprop).permit(:name, :unit_id)
  end

  def sort_column
    Matprop.column_names.include?(params[:sort]) ? params[:sort] : "name"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ?  params[:direction] : "asc"
  end
end
