class GostsController < ApplicationController

  def new
    @gost = Gost.new
  end

  helper_method :sort_column, :sort_direction

  def index
    @gosts = Gost.order(sort_column + ' ' + sort_direction)
  end

  def edit
    @gost = Gost.find(params[:id])
  end

  def show
    @gost = Gost.find(params[:id])
  end

  def create
    #   render plain: params[:propunit].inspect

    @gost = Gost.new(gost_params)

    if @gost.save
      redirect_to @gost
    else
      render 'new'
    end
  end

  def update
    @gost = Gost.find(params[:id])

    if @gost.update(gost_params)
      redirect_to @gost
    else
      render 'edit'
    end
  end

  def destroy
    @gost = Gost.find(params[:id])
    @gost.destroy

    redirect_to gosts_path
  end

  private
  def gost_params
    params.require(:gost).permit(:name,:caption)
  end

  def sort_column
    Gost.column_names.include?(params[:sort]) ? params[:sort] : "name"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ?  params[:direction] : "asc"
  end
end
