class StaticPagesController < ApplicationController
  def home
  end

  def help
  end
  
  def about
  end

  def findmaterial
    @materials = Material.all.order(:name)

    if params[:inputid].nil?
      params[:inputid] = 1
    end

    @matpropvalues = Matpropvalue.where material_id: params[:inputid]
    @material = Material.find(params[:inputid])
  end
end
