class MaterialsController < ApplicationController

  def new
    @material = Material.new
    @gosts = Gost.all.order(:name)
  end

  helper_method :sort_column, :sort_direction

  def index
    @materials = Material.order(sort_column + ' ' + sort_direction)
  end

  def edit
    @material = Material.find(params[:id])
    unless @material.gost_id.nil?
      @gost = Gost.find(@material.gost_id)
    end
    @gosts = Gost.all.order(:name)
  end

  def show
    @material = Material.find(params[:id])
  end

  def create
    #   render plain: params[:propunit].inspect

    @material = Material.new(material_params)

    if @material.save
      redirect_to @material
    else
      render 'new'
    end
  end

  def update
    @material = Material.find(params[:id])

    if @material.update(material_params)
      redirect_to @material
    else
      render 'edit'
    end
  end

  def destroy
    @material = Material.find(params[:id])
    @material.destroy

    redirect_to materials_path
  end

  private
  def material_params
    params.require(:material).permit(:name,:desc,:gost_id)
  end

  def sort_column
    Material.column_names.include?(params[:sort]) ? params[:sort] : "name"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ?  params[:direction] : "asc"
  end
end
