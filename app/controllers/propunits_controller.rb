class PropunitsController < ApplicationController

  def new
    @propunit = Propunit.new
  end

  helper_method :sort_column, :sort_direction

  def index
    @propunit = Propunit.order(sort_column + ' ' + sort_direction)
  end

  def edit
    @propunit = Propunit.find(params[:id])
  end

  def show
    @propunit = Propunit.find(params[:id])
  end

  def create
   # render plain: params[:propunit].inspect

   @propunit = Propunit.new(propunit_params)

   if @propunit.save
     redirect_to @propunit
   else
     render 'new'
   end
  end

  def update
    @propunit = Propunit.find(params[:id])

    if @propunit.update(propunit_params)
      redirect_to @propunit
    else
      render 'edit'
    end
  end

  def destroy
    @propunit = Propunit.find(params[:id])
    @propunit.destroy

    redirect_to propunits_path
  end

  private
  def propunit_params
    params.require(:propunit).permit(:name)
  end

  def sort_column
    Propunit.column_names.include?(params[:sort]) ? params[:sort] : "name"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ?  params[:direction] : "asc"
  end
end
