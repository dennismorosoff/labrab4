# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_07_26_011609) do

  create_table "gosts", force: :cascade do |t|
    t.string "name", null: false
    t.string "caption"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "gostname_unique", unique: true
  end

  create_table "materials", force: :cascade do |t|
    t.string "name", null: false
    t.integer "gost_id"
    t.text "desc"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["gost_id"], name: "gost_id"
    t.index ["name"], name: "materialname_unique", unique: true
  end

  create_table "matprops", force: :cascade do |t|
    t.string "name"
    t.integer "unit_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "matpropname_unique", unique: true
    t.index ["unit_id"], name: "index_matprops_on_unit_id"
  end

  create_table "matpropvalues", force: :cascade do |t|
    t.integer "material_id", null: false
    t.integer "property_id", null: false
    t.float "num_value", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["material_id", "property_id"], name: "material_property_unique", unique: true
    t.index ["material_id"], name: "index_matpropvalues_on_material_id"
    t.index ["property_id"], name: "index_matpropvalues_on_property_id"
  end

  create_table "propunits", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "propunitsname_unique", unique: true
  end

  create_table "user_log", id: false, force: :cascade do |t|
    t.integer "Id_u", null: false
    t.text "u_date", null: false
  end

end
