class CreatePropunits < ActiveRecord::Migration[5.2]
  def change
    create_table :propunits do |t|
      t.string :name

      t.timestamps
    end
  end
end
